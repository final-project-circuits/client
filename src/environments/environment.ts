// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // firebase: {
    
  //   apiKey: "AIzaSyBmNkRGMwyD_Z6sPYY3FtKI4H9w7kUzYi4",
  //   authDomain: "cohesive-apogee-95023.firebaseapp.com",
  //   databaseURL: "https://console.firebase.google.com/project/chabadconnect-6d018/authentication/users",
  //   projectId: "cohesive-apogee-95023",
  //   storageBucket: "cohesive-apogee-95023.appspot.com",
  //   messagingSenderId: "546552200902"
  // }

  firebase: {
    // apiKey: "AIzaSyAPPQBSV_AV-ajmez9uWCxiRNptAzpv2OE",
    // authDomain: "mycircuits-6ce09.firebaseapp.com",
    // databaseURL: "https://mycircuits-6ce09.firebaseio.com",
    // projectId: "mycircuits-6ce09",
    // storageBucket: "mycircuits-6ce09.appspot.com",
    // messagingSenderId: "806628150069"
     apiKey: "AIzaSyBhYernDpo7iKH6zq-oFRDZwdfTMQ7qKdE",
    authDomain: "chabadconnect-6d018.firebaseapp.com",
    databaseURL: "https://chabadconnect-6d018.firebaseio.com",
    projectId: "chabadconnect-6d018",
    storageBucket: "chabadconnect-6d018.appspot.com",
    messagingSenderId: "850058408816"

  }
};


