﻿export class User {
    id: number;
    name: string;
}

export const USERS: User[] = [
    { id: 1, name: "chaya vaizer" },
    { id: 2, name: "chani raich" },
    { id: 3, name: "shira makover" }
]