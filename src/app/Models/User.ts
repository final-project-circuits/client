﻿import { Item } from "./item";
import { Group } from "./Group";
import { IpMachine } from "./IpMachine";

export class User {
    Id: number
    firstName: string;
    lastName: string;
    Birthday: Date;
    phone: string;
    ipMachine: IpMachine;
    item: Item[] = [];
    group: Group[] = [];
}





