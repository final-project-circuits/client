﻿import { Status } from "./Status";
import { Group } from "./Group";

export class Item {
    code: number
    description: string;
    id_user: number;
    id_group: number;
    img: string;
    status: Status[] = [];
    groups: Group[] = [];
    imgBase64: string;
}