﻿import { Item } from "./item";

export class Group {
    code: number
    description: string;
    id_user: number;
    img: string;
    items: Item[] = [];
}
//export class CodeItem {
//    code: number
//    description: string;
//}