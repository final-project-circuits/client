﻿import { Time } from "@angular/common/src/i18n/locale_data_api";


export class Status {
    priority: number;
    code: number;
    description: string;
    code_event: number;
    start: Time;
    end: Time;
    Day: boolean[] = [];
}