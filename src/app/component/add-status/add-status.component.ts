import { Component, OnInit } from '@angular/core';
import { Status } from '../../models/Status';

@Component({
  selector: 'app-add-status',
  templateUrl: './add-status.component.html',
  styleUrls: ['./add-status.component.scss']
})
export class AddStatusComponent{
  eventList: Event[] = [];
  status: Status  = new Status();
  priorityList: Status[] = [];
  sortListP:Status[] = [];
  viewPriority: boolean;
  constructor() { 
 //this.eventList = myService.eventList;
  this.status = new Status();
  this.status.Day = [false, false, false, false, false, false];
 // this.priorityList = this.myService.statusList;
  console.log(this.priorityList);
  this.viewPriority = true;
  this.sortListP = this.priorityList.slice(0);
  this.sortListP.sort(function (a, b) {
      return a.priority - b.priority;
  });
}
  ngOnInit() {
  }

}


