﻿import { Injectable } from "@angular/core";
//import { Http, RequestOptions, Headers } from "@angular/http"
//import { Observable } from "rxjs/Observable"
import { User } from "../models/User"
import { Status } from "../models/Status";
import { Event } from "../models/Event";
//import "rxjs/add/operator/map"
//import { SelectItem } from 'primeng/api';
import { Group } from "../models/Group";
//import { Response } from "@angular/http/src/static_response";
import { Item } from "../models/item";

// @Injectable()
 export class MyService {  
    statusList: Status[];
    //statusList1: SelectItem[];
   // groupList1: SelectItem[];
    eventList: Event[];
    user: User;
    g: Group;
  //  constructor(private http: Http) {
     //   this.user = new User();
      //  this.getAllEventFromServer().subscribe(data => { this.eventList = data }, err => { alert("error event") });       
     }
//     //של המכשיר IP הפונקציה מוצאת ושולפת את המשתמש הנוכחי עפ"י 
//     getUserFromServer(): Observable<User> {
//         return this.http.get("/api/User/getUser1").map(data => { return data.json(); });
//     }
//     // הפונקציה שולפת את הארוע הנכון עפ"י הסטטוס המתאים לפריט עפ"י יום, שעה ועדיפות 
//     getStatus(i: number): Observable<number> {
//         return this.http.get("/api/Status/getStatus?i=" + i).map(data => { return console.log(data.json()), data.json(); });
//     }
//     //הפונקציה שולפת משתמש עפ"י טלפון ויום הולדת
//     findUser(user: User): Observable<User> {
//         return this.http.post("/api/User/findUser",user).map(data => { return console.log(data.json()), data.json(); });
//     }
//     //הפונקציה שולפת את קבוצות המשתמש עפ"י קוד משתמש
//     getGroupsUserFromServer(id: number): Observable<Group[]> {
//         return this.http.get("/api/Group/getGroups?mis=" + id).map(data => { return console.log(data.json()), data.json(); });
//     }
//     //הפונקציה שומרת משתמש חדש
//     putUserToDB(newUser: User,codeEvent:number): Observable<number> {
//         return this.http.post("/api/User/putUserToDB?codeEvent=" + codeEvent, newUser ).map(data => { return data.json(); });
//     }
//     //הפונקציה שומרת שינוי בפריט
//     updateItemToDB(item: Item): Observable<boolean> {
//         return this.http.post("/api/User/updateItemToDB", item).map(data => { return true; });
//     }
//     //הפונקציה שולפת את כל הסטטוסים של המשתמש
//     getAllStatusFromServer(idUser:number): Observable<Status[]> {
//         return this.http.get("/api/Status/getAllStausFromServer?idUser="+idUser).map(data => { return console.log(data.json()), data.json(); });
//     }
//     //הפונקציה שולפת את כל הארועים הקיימים
//     getAllEventFromServer(): Observable<Event[]> {
//         return this.http.get("/api/Event/getAllEventFromServer").map(data => { return console.log(data.json()), data.json(); });
//     }
//     //הפונקציה שומרת פריט חדש למשתמש
//     postNewItemToDB(idUser: number, newItem: Item): Observable<number> {
//         return this.http.post("/api/User/postNewItemToDB?idUser=" + idUser, newItem).map(data => { return data.json(); });
//     }
//     //הפונקציה שומרת סטטוס חדש
//     putStatusToDB(idUser: number, newStatus: Status): Observable<boolean> {
//         return this.http.post("/api/Status/putStatusToDB?idUser=" + idUser, newStatus).map(data => { return true; });
//     }
//     //הפונקציה שומרת קבוצה חדשה למשתמש
//     postNewGroup( newGroup: Group): Observable<number> {
//         return this.http.post("/api/Group/postNewGroup", newGroup).map(data => { return data.json(); });
//     }
//    //הפונקציה שומרת לחפץ קבוצות חדשות
//     postGroupsToNewItem(codeNewItem:number,newGroups: Group[]): Observable<number> {
//         return this.http.post("/api/Group/postGroupsToNewItem?codeNewItem="+codeNewItem, newGroups).map(data => { return data.json(); });
//     }
//     // BYTE[]הפונקציה שומרת תמונה כ
//     imageChange(event: any, index: number): Observable<string> {
//         let fileList: FileList = event.target.files;
//         if (fileList.length == 0)
//             return null;
//         else {
//             let file: File = fileList[0];
//             let formData: FormData = new FormData();
//             formData.append('uploadFile', file, file.name);
//             let headers = new Headers()
//             let options = new RequestOptions({ headers: headers });
//             return  this.http.post("/api/User/UploadJsonFile", formData, options)
//               .map(data => { return data.json() as string});
//         }
//     }   
//     //foo(path: string) {
//     //    return this.http.get("/api/User/downloadImage?path=" + path).toPromise().
//     //        then(data => { return data.json() });
//     //}
// }
