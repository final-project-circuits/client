
import { LoginComponent } from './component/login/login.component';
import { UserComponent } from './component/user/user.component';
import { RegisterComponent } from './component/register/register.component';
import { UserResolver } from './component/user/user.resolver';
import { AuthGuard } from './core/auth.guard';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddStatusComponent } from './component/add-status/add-status.component';
import { AddUserComponent } from './add-user/add-user.component';
import { SearchItemComponent } from './component/search-item/search-item.component';


export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
  { path: 'addStatus', component: AddStatusComponent },
  { path: 'searchItem', component: SearchItemComponent },
  { path: 'user', component: UserComponent,  resolve: { data: UserResolver}},
];
